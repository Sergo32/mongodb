﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Model.Entities;

namespace MongoDB.Model.Entities
{
    class Library
    {

        public ObjectId Id { get; set; }
        public int Count { get; set; }
        public string Style { get; set; }

        public List<Book> Books { get; set; }

        public override string ToString()
        {
            return $"{Style} : {Count} кол-во";
        }
    }
}
