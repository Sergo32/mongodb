﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Model.Entities;
using MongoDB.Driver;
using MongoDB.Bson;

namespace MongoDB.Model
{
    class CollectionLibrary
    {

        private IMongoCollection<Library> mongoCollection;
        public CollectionLibrary()
        {
            string connectionString = "mongodb://localhost";
            MongoClient client = new MongoClient(connectionString);
            IMongoDatabase database = client.GetDatabase("proba");
            mongoCollection = database.GetCollection<Library>("library");


        }

        public List<Library> LoadLibrary()
        {
            return mongoCollection.Find(new BsonDocument()).ToList();
        }

        public void AddNewLibrary(Library library)
        {
            mongoCollection.InsertOne(library);
        }

        public void DeleteLibraryById(ObjectId id)
        {
            mongoCollection.DeleteOne(item => item.Id == id);
        }

        public void UpdateById(ObjectId id,  Library library)
        {
            mongoCollection.ReplaceOne(item => item.Id == id, library);
        }


    }

}
