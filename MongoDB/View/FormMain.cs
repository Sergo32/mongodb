﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.ControllerForm;

namespace MongoDB
{
    public partial class FormMain : Form
    {
        private ControllerFormMain controller; 
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controller = new ControllerFormMain(this);
            controller.FillDataGridViewLibrary();
            controller.FillListBoxBook();
        }

        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            controller.AddLybrary();
            controller.FillDataGridViewLibrary();
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            controller.DeleteLybrary();
            controller.FillDataGridViewLibrary();

        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            controller.UpdateLybrary();
            controller.FillDataGridViewLibrary();

        }

        private void ListBoxLibrary_SelectedIndexChanged(object sender, EventArgs e)
        {
            controller.FillRichTextBoxLybraryBook();
        }
    }
}
