﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Model;
using MongoDB.Model.Entities;

namespace MongoDB.ControllerForm
{
    class ControllerFormMain
    {
        private FormMain form;
        private CollectionLibrary Library;

        public ControllerFormMain(FormMain f)
        {
            form = f;
            Library = new CollectionLibrary();
        }

        public void FillListBoxBook()
        {
            List<Book> books = new List<Book>()
            {
                new Book(){Name="Припять",Price=333},
                 new Book(){Name="Самогон",Price=100},
                  new Book(){Name="Жорик",Price=2200},
                   new Book(){Name="Нрод",Price=450},
                    new Book(){Name="Херм",Price=500}


            };
            form.listBoxBook.DataSource = books;
        }


        public void FillDataGridViewLibrary()
        {
            form.listBoxLibrary.DataSource = null;
            form.listBoxLibrary.DataSource = Library.LoadLibrary();
        }

        public void AddLybrary()
        {
            string style = form.textBoxName.Text;
            int count = int.Parse(form.textBoxCount.Text);
            List<Book> Book = form.listBoxBook.SelectedItems.Cast<Book>().ToList();

            Library library = new Library()
            {
                Style = style,
                Count = count,
                Books = Book
            };

            Library.AddNewLibrary(library);
        }

        public void FillRichTextBoxLybraryBook()
        {
            form.richTextBoxLibraryBook.Clear();

            Library library = (Library)form.listBoxLibrary.SelectedItem;


            if (library == null)
            {
                return;
            }
            foreach (Book book in library.Books)
            {
                form.richTextBoxLibraryBook.Text += book + "\n";
            }
        }

        public void DeleteLybrary()
        {
            Library library = (Library)form.listBoxLibrary.SelectedItem;


            if (library == null)
            {
                return;
            }
            Library.DeleteLibraryById(library.Id);
        }

        public void UpdateLybrary()
        {
            Library upddLibrary = (Library)form.listBoxLibrary.SelectedItem;


            if (upddLibrary == null)
            {
                return;
            }

            string style = form.textBoxName.Text;
            int cont = int.Parse(form.textBoxCount.Text);
            List<Book> cars = form.listBoxBook.SelectedItems.Cast<Book>().ToList();

            Library library= new Library()
            {
                Id = upddLibrary.Id,
                Style = style,
                 Count = cont,
                Books = cars
            };

            Library.UpdateById(upddLibrary.Id, library);
        }
    }
}
