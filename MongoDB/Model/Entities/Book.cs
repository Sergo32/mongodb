﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MongoDB.Model.Entities
{
    class Book
    {
        public int Price { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return $"{Name} : {Price}";
        }

    }
}
